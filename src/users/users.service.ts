import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234' },
  { id: 2, login: 'user1', name: 'User 1', password: 'Pass@1234' },
  { id: 3, login: 'user2', name: 'User 2', password: 'Pass@1234' },
];
let lastUserId = 4; //เพิ่มขึ้นตาม

@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    // const newUser = new User();
    // //การเพิ่มuserใหม่ เเละพุช --> postman
    // newUser.id = lastUserId++; //เพิ่มไอดี=เพิ่มuser
    // newUser.login = createUserDto.login;
    // newUser.name = createUserDto.name;
    // newUser.password = createUserDto.password;
    console.log({ ...createUserDto });
    const newUser: User = {
      //การระเบิดcreateUserDtoออกมา เเละเพิ่มuserใหม่ -แทนข้างบน
      id: lastUserId++,
      ...createUserDto, //ระเบิดlogin name password
    };
    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    //ถ้ามันไม่มี จะเจ้งnot found
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }
  //patch
  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    //ถ้ามันไม่มี จะเจ้งnot found
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user' + JSON.stringify(users[index]));
    // console.log('update' + JSON.stringify(updateUserDto));
    const updateUser: User = {
      ...users[index], //จะได้id name login password
      ...updateUserDto, //  จะทับของเดิม
    };
    users[index] = updateUser; //แทนที่indexเดิม
    return updateUser;
  }
  //delete
  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    //ถ้ามันไม่มี จะเจ้งnot found
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = users[index];
    users.splice(index, 1);
    return deletedUser;
  }
  reset() {
    users = [
      { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234' },
      { id: 2, login: 'user1', name: 'User 1', password: 'Pass@1234' },
      { id: 3, login: 'user2', name: 'User 2', password: 'Pass@1234' },
    ];
    lastUserId = 4;
    return 'Reset';
  }
}
